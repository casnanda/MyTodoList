//
//  FirstViewController.swift
//  MyTodoList
//
//  Created by caq on 14-10-1.
//  Copyright (c) 2014年 caq. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        addTodo("起床", "7点准时起床")
        addTodo("跑步", "跑步锻炼身体")
        addTodo("买早餐", "买一包面包")
        addTodo("午休", "中午休息半个小时")
        addTodo("下班", "下班走路回家")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todoList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "Default")
        let todo = todoList[indexPath.row]
        cell.textLabel?.text = todo.name
        cell.detailTextLabel?.text = todo.desc
        if todo.stat {
            cell.accessoryType = .Checkmark
        } else {
            cell.accessoryType = .None
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            removeTodoAtIndex(indexPath.row)
            tableView.reloadData()//table需显式重新加载，不会自动重载的
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let todo = todoList[indexPath.row]
        println("*****\(todo.stat)*****")
        todo.stat = !todo.stat
        println("#####\(todo.stat)#####")
        tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }


}

