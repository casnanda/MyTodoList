//
//  SecondViewController.swift
//  MyTodoList
//
//  Created by caq on 14-10-1.
//  Copyright (c) 2014年 caq. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var todoName: UITextField!
    @IBOutlet weak var todoDesc: UITextField!
    @IBAction func addNew(sender: UIButton) {
        self.view.endEditing(true)//告诉系统输入结束了，收起键盘
        if todoName.text.isEmpty || todoDesc.text.isEmpty {
            let alert = UIAlertView(title: "警告", message: "名称和描述均不能为空！", delegate: nil, cancelButtonTitle: "确定")//IOS8推荐使用UIAlertController with a preferredStyle of UIAlertControllerStyleActionSheet.
            alert.show()
        } else {
            addTodo(todoName.text, todoDesc.text)
            todoName.text = ""
            todoDesc.text = ""
            self.tabBarController?.selectedIndex = 0//切换tab显示的界面
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //点击输入框以外的地方触发
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)//告诉系统输入结束了，收起键盘
    }
    
    //点击键盘的return时收起键盘
    //UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }


}

