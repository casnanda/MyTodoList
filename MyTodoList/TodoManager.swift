//
//  TodoManager.swift
//  MyTodoList
//
//  Created by caq on 14-10-1.
//  Copyright (c) 2014年 caq. All rights reserved.
//

import Foundation

var todoList = [Todo]()

class Todo {
    var name = "unamed"
    var desc = "undesc"
    var stat = false
    
    init(name: String, desc: String, stat: Bool) {
        self.name = name
        self.desc = desc
        self.stat = stat
    }
}

func addTodo(name: String, desc: String, stat: Bool) {
    todoList.append(Todo(name: name, desc: desc, stat: stat))
}

func addTodo(name: String, desc: String) {
    addTodo(name, desc, false)
}

func removeTodoAtIndex(index: Int) {
    todoList.removeAtIndex(index)
}